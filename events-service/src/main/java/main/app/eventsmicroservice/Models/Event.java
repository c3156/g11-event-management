package main.app.eventsmicroservice.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "type", nullable = false)
    private String type;
    @Column(name = "location", nullable = false)
    private String location;
    @Column(name = "date", nullable = false)
    private String date;
    @Column(name = "image", nullable = false)
    private String image;
    @Column(name = "price", nullable = false)
    private Long price;
    @Column(name = "organizer_id", nullable = false)
    private Long organizerId;

    public Event() {
    }

    public Event(String name, String description, String type, String location, String image, Long price, String date,
            Long organizerId) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.location = location;
        this.image = image;
        this.date = date;
        this.price = price;
        this.organizerId = organizerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(Long organizerId) {
        this.organizerId = organizerId;
    }

    @Override
    public String toString() {
        return "Event [name=" + name + ", description=" + description + ", type=" + type + ", location=" + location
                + ", date=" + date + ", image=" + image + ", price=" + price + ", organizerId=" + organizerId + "]";
    }

}
