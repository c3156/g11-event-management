package main.app.eventsmicroservice.Repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import main.app.eventsmicroservice.Models.Event;

public interface EventsRepository extends JpaRepository<Event, Long> {
    Optional<List<Event>> findByOrganizerId(Long organizerId);
}
