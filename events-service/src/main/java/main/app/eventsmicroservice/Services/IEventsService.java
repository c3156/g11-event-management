package main.app.eventsmicroservice.Services;

import java.util.List;

import main.app.eventsmicroservice.Models.Event;

public interface IEventsService {

    public List<Event> getEvents();

    public Event getEventById(Long id);

    public List<Event> getEventsByOrganizerId(Long organizerId);

    public List<Event> getEventsByUserId(Long userId);

    public String addEvent(Event event);

    public String updateEvent(Event event);

    public String deleteEvent(Long id);
}
