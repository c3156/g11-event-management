package main.app.eventsmicroservice.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.http.HttpMethod;
import org.springframework.core.ParameterizedTypeReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.core.env.Environment;

import main.app.eventsmicroservice.Models.Event;
import main.app.eventsmicroservice.Repositories.EventsRepository;

@Service
public class EventsService implements IEventsService {

    @Autowired
    private EventsRepository eventsRepository;

    @Autowired
    private Environment env;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Event> getEvents() {
        return eventsRepository.findAll();
    }

    @Override
    public Event getEventById(Long id) {
        try {
            return eventsRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public List<Event> getEventsByUserId(Long id) {
        String envUrl = env.getProperty("app.url");
        String url = "";
        if (envUrl.equals("http://localhost:3001/data"))
            url = envUrl;
        else
            url = envUrl + id;
        try {

            ResponseEntity<List<Long>> response = restTemplate.exchange(url,
                    HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Long>>() {
                    });

            if (response.getBody().isEmpty()) {
                return new ArrayList<>();
            }

            List<Long> eventIds = response.getBody();
            List<Event> eventsJoinedByTheUser = eventsRepository.findAllById(eventIds);
            if (eventsJoinedByTheUser == null) {
                return new ArrayList<Event>();
            }
            return eventsJoinedByTheUser;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }

    }

    @Override
    public List<Event> getEventsByOrganizerId(Long organizerId) {
        try {
            return eventsRepository.findByOrganizerId(organizerId).get();
        } catch (NoSuchElementException e) {
            return new ArrayList<Event>();
        }
    }

    @Override
    public String addEvent(Event event) {
        try {
            eventsRepository.save(event);
            return "Event added successfully";
        } catch (Exception e) {
            return "Error adding event";
        }
    }

    @Override
    public String updateEvent(Event event) {
        try {
            Event eventToUpdate = eventsRepository.findById(event.getId()).get();
            eventToUpdate.setName(event.getName());
            eventToUpdate.setDescription(event.getDescription());
            eventToUpdate.setDate((event.getDate()));
            eventToUpdate.setType(event.getType());
            eventToUpdate.setLocation(event.getLocation());
            eventToUpdate.setImage(event.getImage());
            eventToUpdate.setPrice(event.getPrice());
            eventToUpdate.setOrganizerId(event.getOrganizerId());
            eventsRepository.save(eventToUpdate);
            return "Event updated successfully";
        } catch (Exception e) {
            return "Error updating event";
        }
    }

    @Override
    public String deleteEvent(Long id) {
        try {
            eventsRepository.deleteById(id);
            return "Event deleted successfully";
        } catch (Exception e) {
            return "Error deleting event";
        }
    }

}
