package main.app.eventsmicroservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import main.app.eventsmicroservice.Models.Event;
import main.app.eventsmicroservice.Services.EventsService;

@RestController
@RequestMapping("/events")
public class EventsController {

    @Autowired
    private EventsService eventsService;

    @GetMapping("")
    public List<Event> getEvents() {
        return eventsService.getEvents();
    }

    @GetMapping("/{id}")
    public Event getEventById(@PathVariable(name = "id") Long id) {
        return eventsService.getEventById(id);
    }

    @GetMapping("/getEventsByUserId/{id}")
    public List<Event> getEventsByUserId(@PathVariable(name = "id") Long id) {
        return eventsService.getEventsByUserId(id);
    }

    @GetMapping("/getEventsByOrganizerId/{organizerId}")
    @CrossOrigin(origins = "*")
    public List<Event> getEventsByOrganizerId(@PathVariable(name = "organizerId") Long organizerId) {
        return eventsService.getEventsByOrganizerId(organizerId);
    }

    @PostMapping(value = "", consumes = "application/json")
    public String addEvent(@RequestBody Event event) {
        return eventsService.addEvent(event);
    }

    @PutMapping("")
    public String updateEvent(@RequestBody Event event) {
        return eventsService.updateEvent(event);
    }

    @DeleteMapping("/{id}")
    public String deleteEvent(@PathVariable(name = "id") Long id) {
        return eventsService.deleteEvent(id);
    }
}
