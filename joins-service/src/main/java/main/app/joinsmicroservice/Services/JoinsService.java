package main.app.joinsmicroservice.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import ch.qos.logback.core.joran.conditional.ElseAction;
import main.app.joinsmicroservice.Models.Joins;
import main.app.joinsmicroservice.Repo.JoinsRepo;

@Service
public class JoinsService {
    @Autowired
    private JoinsRepo joinsRepo;
    public String joinEvent(Long idEvent,Long idUser)
    {
        //check user and event existence
        //if success put to database and return success
        //else return failed

        Joins join = joinsRepo.save(new Joins(idUser,idEvent));
        if(join==null) return "error";
        return "success";        
    }

    public List<Long> findEventsByUserId(Long idUser)
    {
        Optional <List<Joins>> events=joinsRepo.findJoinsByUserId(idUser);
        ArrayList<Long> idsEvents= new ArrayList<>();
        if (events.isEmpty()) return idsEvents ;
        for(Joins id:events.get())
        {
            idsEvents.add(id.getEventId());
        }
        return idsEvents;
    }
}
