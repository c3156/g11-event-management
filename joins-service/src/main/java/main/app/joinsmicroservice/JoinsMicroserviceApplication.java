package main.app.joinsmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JoinsMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JoinsMicroserviceApplication.class, args);
	}

}
