package main.app.joinsmicroservice.Repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import main.app.joinsmicroservice.Models.Joins;

public interface JoinsRepo extends JpaRepository<Joins,Long> {
    public Optional<List<Joins>> findJoinsByUserId(Long userId);
}
