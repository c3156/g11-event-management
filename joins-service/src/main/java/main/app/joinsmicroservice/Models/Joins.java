package main.app.joinsmicroservice.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
// @Table(name="mourad_table")
@IdClass(IdsJoins.class)
public class Joins {
    @Id
    private Long userId;
    @Id
    private Long eventId;
    public Joins ()
    {
        
    }
    public Joins(Long userId, Long eventId) {
        this.userId = userId;
        this.eventId = eventId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getEventId() {
        return eventId;
    }
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
}
