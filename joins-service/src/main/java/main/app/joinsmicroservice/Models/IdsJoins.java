package main.app.joinsmicroservice.Models;

import java.io.Serializable;

public class IdsJoins implements Serializable {
    private Long userId;
    private Long eventId;
    public IdsJoins(){
    }
    public IdsJoins(Long userId, Long eventId) {
        this.userId = userId;
        this.eventId = eventId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getEventId() {
        return eventId;
    }
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
    
}
