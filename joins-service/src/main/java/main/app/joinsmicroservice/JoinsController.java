package main.app.joinsmicroservice;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import main.app.joinsmicroservice.Models.Joins;
import main.app.joinsmicroservice.Services.JoinsService;



@RestController
//@RequestMapping("/")
public class JoinsController {
    @Autowired
    private JoinsService joinsService;

    @PostMapping("/joinEvent")
    public String joinEvent(@RequestBody Joins joins){
        return joinsService.joinEvent(joins.getEventId(), joins.getUserId());
    }
    @GetMapping("/seeEvents/{userId}")
    public List<Long> getAllEvents(@PathVariable(name = "userId") Long userId){
        return joinsService.findEventsByUserId(userId);
    }
}
