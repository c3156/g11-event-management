package main.app.usersmicroservice.Repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import main.app.usersmicroservice.Models.User;

public interface UsersRepository extends JpaRepository<User, Long>{
    public Optional<User> findUserByEmail(String email);
}
