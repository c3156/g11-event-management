package main.app.usersmicroservice.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.core.env.Environment;

import main.app.usersmicroservice.Models.User;
import main.app.usersmicroservice.Models.Event;
import main.app.usersmicroservice.Repo.UsersRepository;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Environment env;

    public String login(String email, String password) {
        Optional<User> userOp = usersRepository.findUserByEmail(email);
        if (userOp.isEmpty())
            return "User not found. Please register.";
        if (userOp.get().getPassword().equals(password))
            return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
        return "error";
    }

    public String register(String name, String email, String password, Long number) {
        // create new user
        Optional<User> userOp = usersRepository.findUserByEmail(email);
        if (userOp.isPresent())
            return "User already exists.";
        User user = new User(name, email, number, password);

        try {
            usersRepository.save(user);
            return ("User registered");
        } catch (IllegalArgumentException e) {
            return ("error");
        }

    }

    public List<Event> getEventsByUser(Long userId) {

        String url = env.getProperty("app.url");

        if (url == null)
            return new ArrayList<Event>();

        try {

            ResponseEntity<List<Event>> response = restTemplate.exchange(url + userId,
                    HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Event>>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            return new ArrayList<Event>();
        }

    }
}
