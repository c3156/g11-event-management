package main.app.usersmicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

import main.app.usersmicroservice.Models.User;
import main.app.usersmicroservice.Models.Event;
import main.app.usersmicroservice.Services.UsersService;

@RestController
public class UsersController {
    @Autowired
    private UsersService usersService;

    @PostMapping("/login")
    public String login(@RequestBody User user) {
        return (usersService.login(user.getEmail(), user.getPassword()));
    }

    @PostMapping("/register")
    public String register(@RequestBody User user) {
        return (usersService.register(user.getName(), user.getEmail(), user.getPassword(), user.getNumber()));
    }

    @GetMapping("/getEventsByUser/{userId}")
    public List<Event> getEventsByUser(@PathVariable("userId") Long userId) {
        return (usersService.getEventsByUser(userId));
    }
}
